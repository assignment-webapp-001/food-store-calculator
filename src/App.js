import './App.css';
import Calculator from "./components/Calculator/Calculator";

function App() {
    return (
        <div className="App">
            <div className="App-header">
                <p>
                    Food Store Calculator
                </p>
            </div>
            <Calculator/>
        </div>
    )
}

export default App;
