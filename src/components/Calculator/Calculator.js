import React, {useCallback, useEffect, useState} from 'react';
import styled from "styled-components";

const Body = styled.div`
  position: relative;
  width: 100%;

  form {
    background-color: #fff;
    width: 300px;
    padding: 10px;
    margin: 20px auto;
    -webkit-box-shadow: 0 0 5px rgba(170, 169, 169, .3);
    box-shadow: 0 0 5px rgba(170, 169, 169, .3);
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
  }

  legend {
    padding-bottom: 14px;
    text-align: left;
  }

  fieldset {
    margin-bottom: 14px;
    padding-bottom: 14px;
  }

  fieldset, input[type="button"] {
    border: 0;
  }

  input[type="button"] {
    background-color: #ED5A48;
    color: #fff;
    cursor: pointer;
    width: 24px;
    height: 24px;
  }

  input[type="text"] {
    border: 1px solid #F4F3F3;
    height: 22px;
    width: 50px;
  }

  .price {
    padding-bottom: 20px;
    font-size: 18px;

    &:last-child {
      padding-bottom: 100px;
    }
  }
`
const menus = [{
    id: 'red', label: 'Red set: 50 THB/set'
}, {
    id: 'green', label: 'Green set: 40 THB/set'
}, {
    id: 'blue', label: 'Blue set: 30 THB/set'
}, {
    id: 'yellow', label: 'Yellow set: 50 THB/set'
}, {
    id: 'pink', label: 'Pink set: 80 THB/set'
}, {
    id: 'purple', label: 'Purple set: 90 THB/set'
}, {
    id: 'orange', label: 'Orange set: 120 THB/set'
}]

const Calculator = (props) => {

    const [list, setList] = useState({
        red: {qty: 0, price: 50},
        green: {qty: 0, price: 40},
        blue: {qty: 0, price: 30},
        yellow: {qty: 0, price: 50},
        pink: {qty: 0, price: 80},
        purple: {qty: 0, price: 90},
        orange: {qty: 0, price: 120}
    })
    const [memberDiscount, setMemberDiscount] = useState(false)
    const [discount, setDiscount] = useState(0)
    const [memDiscount, setMemDiscount] = useState(0)
    const [summary, setSummary] = useState(0)
    const [total, setTotal] = useState(0)

    function onAddItem(id) {
        let value = parseInt(document.getElementById(id).value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        updateData(id, value)
    }

    function onDecreaseItem(id) {
        let value = parseInt(document.getElementById(id).value, 10);
        value = isNaN(value) ? 0 : value;
        value = value < 1 ? 1 : value;
        value--;
        updateData(id, value)
    }

    function updateData(id, value) {
        setList({
            ...list, [id]: {
                ...list[id], qty: value
            }
        })
    }

    const calSummary = useCallback(() => {
        let total = 0
        let discount = 0
        let member = 0
        const listDiscount = ['orange', 'pink', 'green']
        menus.map(i => {
            if (list[i.id].qty > 0) {
                if (i.id === 'orange' && list[i.id].qty) {
                    discount = discount + ((list[i.id].qty * list[i.id].price) * (5 / 100))
                }
                if (listDiscount.includes(i.id) && list[i.id].qty > 2) {
                    if (list[i.id].qty % 2 > 0) {
                        const _qty = (Math.floor(list[i.id].qty / 2) * 2)
                        const _discountPrice = (_qty * list[i.id].price) * (5 / 100)
                        discount = discount + _discountPrice
                        total = total + (((_qty * list[i.id].price) - _discountPrice) + list[i.id].price)
                    } else {
                        discount = discount + ((list[i.id].qty * list[i.id].price) * (5 / 100))
                        total = total + (list[i.id].qty * list[i.id].price)
                    }
                } else {
                    total = total + (list[i.id].qty * list[i.id].price)
                }
            }
        })
        if (memberDiscount) {
            member = member + ((total - discount) * (10 / 100))
        }
        setMemDiscount(member.toFixed(2))
        setDiscount(discount.toFixed(2))
        setSummary(total.toFixed(2))
        setTotal((total - discount - member).toFixed(2))
    }, [list, memberDiscount])

    useEffect(() => {
        calSummary()
    }, [calSummary, list, memberDiscount]);

    return (<Body>
        <form>
            {menus && menus.map(((item, key) => (<fieldset key={key}>
                <legend>{item.label}</legend>
                <input type="button" value="-" className="decrease" onClick={() => onDecreaseItem(item.id)}/>
                <input type="text" id={item.id} value={list[item.id].qty} readOnly/>
                <input type="button" value="+" className="increase" onClick={() => onAddItem(item.id)}/>
            </fieldset>)))}
            <div>
                <input type="checkbox" defaultChecked={memberDiscount} id="memberDiscount"
                       onChange={() => setMemberDiscount(!memberDiscount)}
                       title='Is Member'/>
                <label htmlFor="memberDiscount">Customers have a member card.</label>
            </div>
        </form>
        <div className='price'>
            Total before discount: <b>{summary}</b> THB
        </div>
        {memberDiscount && (<div className='price'>
            Member Discount (10%): <b>{memDiscount}</b> THB
        </div>)}
        <div className='price'>
            Order Discount: <b>{discount}</b> THB
        </div>
        <div className='price'>
            Total: <b>{total}</b> THB
        </div>
    </Body>);
}

export default Calculator;
